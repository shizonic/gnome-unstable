# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
options=(debug !strip)

pkgname=baobab
pkgver=40.beta
pkgrel=1
pkgdesc="A graphical directory tree analyzer"
url="https://wiki.gnome.org/Apps/DiskUsageAnalyzer"
arch=(x86_64)
license=(GPL2)
depends=(dconf gtk3 gsettings-desktop-schemas libhandy librsvg)
makedepends=(vala gobject-introspection yelp-tools appstream-glib git meson)
groups=(gnome)
_commit=1b21811fca611a5eafa9ca962fa7800b969e7e54  # tags/40.beta^0
source=("git+https://gitlab.gnome.org/GNOME/baobab.git#commit=$_commit"
        no-mimetype.diff)
sha256sums=('SKIP'
            '958a85fe3a2001d16ebcf71b51888f260508af87224a0de3410a6bbe685e59d0')

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/-/+/g'
}

prepare() {
  cd $pkgname

  # https://bugs.archlinux.org/task/31861
  git apply -3 ../no-mimetype.diff
}

build() {
  arch-meson $pkgname build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  DESTDIR="$pkgdir" meson install -C build
}
